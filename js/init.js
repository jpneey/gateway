$(function(){
    $('#modal1').modal({
      dismissible: false,
      onCloseEnd: checkHuman 
    });
    makeid(5)
})
var request;
var human = false; 

$("[id=ajax-form]").submit(function(event) {

  
  event.preventDefault();
  if (request) {
        request.abort();
  }
  customcaptcha();
});

function sendAjax() {
  
  var $form = $('#ajax-form');
  var $url = $('#ajax-form').attr('action');
  var $inputs = $form.find("input, select, button, textarea");
  var serializedData = $form.serialize();
  
  $inputs.prop("disabled", true);
  $('#ajax-form').css({
    'opacity' : '0.1',
    'cursor' : 'wait'
  })
  
  $form.find("input[type=submit]").val("sending");
  window.onbeforeunload = function() {
    return 'Are you sure you want to navigate away from this page?';
  };
  
  request = $.ajax({
        url: $url,
        type: "post",
        data: serializedData
  });
  
  request.done(function (response, textStatus, jqXHR){
    
    window.onbeforeunload = null;
    M.toast({html: response, classes: 'green'});
    $inputs.val('');
    $form.find("input[type=submit]").val("submit");
    $('#ajax-form').css({
      'opacity' : '1',
      'cursor' : 'initial'
    })
    setTimeout(function(){
      M.toast({html: 'Thank you for reaching out!', classes: 'green'});
    }, 1500)
  });
    
  request.fail(function (jqXHR, textStatus, errorThrown){
  
    console.error(
      "The following error occurred: "+
      textStatus, errorThrown
    );
    
    window.onbeforeunload = null;
  
  });

  request.always(function () {
    $('#ajax-form').css({
      'opacity' : '1',
      'cursor' : 'initial'
    })
    $inputs.prop("disabled", false);
    window.onbeforeunload = null;
  
  });
}


function customcaptcha() {
  $('#modal1').modal('open');
}

function checkHuman() {
  var q = $('#q').text();
  var a = $('#a').val();

  
  var em = $('#em').val();
  var emb = $('#em-b').val();
  
  if (a === q & em === emb) {
    human = true; 
    
    M.toast({html: 'Your email will be sent shortly!', classes: 'green'  });
    sendAjax();
  } 
  else if (em != emb) {
    M.toast({html: 'Email confirmation does not match! :(', classes: 'red'  });
  }

  else {
    M.toast({html: 'Your email can\'t be send! Captcha was invalid!', classes: 'red'  });
  }

}
function makeid(length) {
  var result           = '';
  var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
     result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  $('#q').text(result);
  /* return result; */
}
